class AddPictureToSlide < ActiveRecord::Migration[5.0]
def up
   add_attachment :slides, :picture
 end
def down
   remove_attachment :slides, :picture
 end
end
