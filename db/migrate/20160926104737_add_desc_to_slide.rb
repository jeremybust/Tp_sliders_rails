class AddDescToSlide < ActiveRecord::Migration[5.0]
  def change
    add_column :slides, :desc_left, :text
    add_column :slides, :desc_right, :text
  end
end
