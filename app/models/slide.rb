class Slide < ApplicationRecord
 validates :name, presence: true
 has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
 validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/

 # on crée 2 scopes pour simplifier la synthaxe du controller
  # on recherche que les publication à 'true'
  scope :publication, -> { where(published: true) }
  # on cherche les publications dont la date du jour est comprise dans les dates de publication
  scope :min_publication, -> { where('DateTime(published_from) < ? ', DateTime.now ) }
  scope :max_publication, -> { where('DateTime(published_to) > ?', DateTime.now ) }
end
