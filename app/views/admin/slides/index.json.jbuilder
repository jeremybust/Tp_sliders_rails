json.array!(@slides) do |slide|
  json.extract! slide, :name, :desc_left, :desc_right, :picture
  json.url slide_url(slide, format: :json)
end